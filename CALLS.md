## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Alkira. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Alkira.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Microsoft Teams. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createNotificationWithOptions(body, webhookKey, callback)</td>
    <td style="padding:15px">Create notification in msteams.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdMembers(id, orderby, select, expand, callback)</td>
    <td style="padding:15px">Get members from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdMemberRef(id, body, callback)</td>
    <td style="padding:15px">Add a member to an Office 365 Group through the members navigation property.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members/$ref?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdMembersIdRef(id, memberId, callback)</td>
    <td style="padding:15px">Remove a member from a group via the members navigation property.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members/{pathv2}/$ref?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdGetMemeberGroups(id, body, callback)</td>
    <td style="padding:15px">Return all the groups that the specified group is a member of.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/getMemberGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdGetMemberObjects(id, body, callback)</td>
    <td style="padding:15px">Return all of the groups that this group is a member of.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/getMemberObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdCheckMemberGroups(id, body, callback)</td>
    <td style="padding:15px">Check for membership in the specified list of groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/checkMemberGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupIdOwnersRef(id, body, callback)</td>
    <td style="padding:15px">Add a user to the group's owners.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/owners/$ref?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putGroupsIdTeam(id, body, callback)</td>
    <td style="padding:15px">Create a new team under a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/team?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(top, search, orderby, select, callback)</td>
    <td style="padding:15px">Get entities from groups</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroups(body, callback)</td>
    <td style="padding:15px">Add new entity to groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsId(id, select, callback)</td>
    <td style="padding:15px">Get entity from groups by key</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsId(id, callback)</td>
    <td style="padding:15px">Delete entity from groups</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupsId(id, body, callback)</td>
    <td style="padding:15px">Update entity in groups</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdDrive(id, select, expand, callback)</td>
    <td style="padding:15px">Get drive from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/drive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdPlannerPlans(id, orderby, select, expand, callback)</td>
    <td style="padding:15px">Get plans from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/planner/plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdOnenoteNotebooks(id, orderby, select, expand, callback)</td>
    <td style="padding:15px">Get notebooks from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/onenote/notebooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdOnenoteNotebooks(id, body, callback)</td>
    <td style="padding:15px">Create new navigation property to notebooks for groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/onenote/notebooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdOnenotePages(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of page objects.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/onenote/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdOnenotePages(id, bodyFormData, callback)</td>
    <td style="padding:15px">Create a new OneNote page in the default section of the default notebook.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/onenote/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdOnenoteSectionGroups(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of sectionGroup objects.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/onenote/sectionGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdOnenoteSections(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of onenoteSection objects.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/onenote/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdCalendarEvents(id, orderby, select, callback)</td>
    <td style="padding:15px">Get events from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/calendar/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdCalendarEvents(id, body, callback)</td>
    <td style="padding:15px">Create new navigation property to events for groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/calendar/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdCalendarEventsId(id, eventId, select, callback)</td>
    <td style="padding:15px">Get events from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/calendar/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdCalendarEventsId(id, eventId, callback)</td>
    <td style="padding:15px">Delete event.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/calendar/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupsIdCalendarEventsId(id, eventId, body, callback)</td>
    <td style="padding:15px">Update the navigation property events in groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/calendar/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdEvents(id, orderby, select, callback)</td>
    <td style="padding:15px">Get events from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdEvents(id, body, callback)</td>
    <td style="padding:15px">Create new navigation property to events for groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdEventsId(id, eventId, select, callback)</td>
    <td style="padding:15px">Get events from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdEventsId(id, eventId, callback)</td>
    <td style="padding:15px">Delete event.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupsIdEventsId(id, eventId, body, callback)</td>
    <td style="padding:15px">Update the navigation property events in groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdEventsIdExtensions(id, eventId, body, callback)</td>
    <td style="padding:15px">Create new navigation property to extensions for groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events/{pathv2}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdEventsIdExtensionsId(id, eventId, extensionId, select, expand, callback)</td>
    <td style="padding:15px">Get extensions from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/events/{pathv2}/extensions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdExtensions(id, body, callback)</td>
    <td style="padding:15px">Create new navigation property to extensions for groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdExtensionsId(id, extensionId, select, expand, callback)</td>
    <td style="padding:15px">Get extensions from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/extensions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdCalendarView(id, startDateTime, endDateTime, orderby, select, callback)</td>
    <td style="padding:15px">Get calendarView from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/calendarView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdMemberof(orderby, select, expand, id, callback)</td>
    <td style="padding:15px">Get groups that the group is a direct member of.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/memberOf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdOwners(id, orderby, select, expand, callback)</td>
    <td style="padding:15px">Retrieve a list of the group's owners.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/owners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdRenew(id, callback)</td>
    <td style="padding:15px">Invoke action renew.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdSettings(id, orderby, select, expand, callback)</td>
    <td style="padding:15px">Get settings from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdSettings(id, body, callback)</td>
    <td style="padding:15px">Create new navigation property to settings for groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdSettingsId(id, settingId, select, expand, callback)</td>
    <td style="padding:15px">Get a settings from groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/settings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdSettingsId(id, settingId, callback)</td>
    <td style="padding:15px">Delete a group setting.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/settings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupsIdSettingsId(id, settingId, body, callback)</td>
    <td style="padding:15px">Update the navigation property settings in groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/settings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdConversations(id, orderby, select, callback)</td>
    <td style="padding:15px">Get conversations from groups</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/conversations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdConversations(id, body, callback)</td>
    <td style="padding:15px">Create new navigation property to conversations for groups</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/conversations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdConversationsId(id, conversationId, select, callback)</td>
    <td style="padding:15px">Get conversations from groups</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/conversations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdConversationsId(id, conversationId, callback)</td>
    <td style="padding:15px">Delete a conversation object.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/conversations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdThreads(id, select, orderby, callback)</td>
    <td style="padding:15px">Get all the threads of a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/threads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsIdThreads(id, body, callback)</td>
    <td style="padding:15px">Start a new group conversation by first creating a thread.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/threads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdThreadsId(id, threadId, select, orderby, callback)</td>
    <td style="padding:15px">Get a thread object.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/threads/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdThreadsId(id, threadId, callback)</td>
    <td style="padding:15px">Delete a thread object.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/threads/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupsIdThreadsId(id, threadId, body, callback)</td>
    <td style="padding:15px">Update a thread object.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/threads/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupsIdOwnersIdRef(id, ownerId, callback)</td>
    <td style="padding:15px">Remove an owner from an Office 365 group through the owners navigation property.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/owners/{pathv2}/$ref?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdDriveItemsId(id, itemId, expand, select, callback)</td>
    <td style="padding:15px">Retrieve the metadata for a DriveItem in a Drive by file system ID.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/drive/items/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdDriveRootDelta(id, expand, select, top, callback)</td>
    <td style="padding:15px">Track changes to a drive and its children over time.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/drive/root/delta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsIdDriveItemsIdChildren(id, itemId, expand, orderby, select, skipToken, top, callback)</td>
    <td style="padding:15px">Return a collection of DriveItems in the children relationship of a DriveItem.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/drive/items/{pathv2}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsId(id, select, expand, callback)</td>
    <td style="padding:15px">Retrieve the properties and relationships of the specified team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTeamsId(id, body, callback)</td>
    <td style="padding:15px">Update the properties of the specified team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsIdClone(id, body, callback)</td>
    <td style="padding:15px">Create a copy of a team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsIdArchive(id, body, callback)</td>
    <td style="padding:15px">Archive the specified team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsIdUnarchive(id, callback)</td>
    <td style="padding:15px">Restore an archived team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/unarchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppCatalogsTeamsApps(filter, select, expand, callback)</td>
    <td style="padding:15px">List apps from the Microsoft Teams app catalog.</td>
    <td style="padding:15px">{base_path}/{version}/appCatalogs/teamsApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppCatalogsTeamApps(body, callback)</td>
    <td style="padding:15px">Publish an app to the Microsoft Teams apps catalog.</td>
    <td style="padding:15px">{base_path}/{version}/appCatalogs/teamsApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAppCatalogsTeamsAppsId(id, body, callback)</td>
    <td style="padding:15px">Update an app previously published to the Microsoft Teams app catalog.</td>
    <td style="padding:15px">{base_path}/{version}/appCatalogs/teamsApps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppCatalogsTeamAppsId(id, callback)</td>
    <td style="padding:15px">Remove the app from your organization's app catalog (the tenant app catalog).</td>
    <td style="padding:15px">{base_path}/{version}/appCatalogs/teamsApps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsIdInstalledApps(id, callback)</td>
    <td style="padding:15px">Retrieve the list of apps installed in the specified team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/installedApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsIdInstalledApps(id, body, callback)</td>
    <td style="padding:15px">Installs an app to the specified team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/installedApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsIdChannelsIdTabs(id, channelId, callback)</td>
    <td style="padding:15px">Retrieve the list of tabs in the specified channel within a team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/tabs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsIdChannelsIdTabs(id, channelId, body, callback)</td>
    <td style="padding:15px">Adds (pins) a tab to the specified channel within a team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/tabs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsIdChannels(id, callback)</td>
    <td style="padding:15px">Retrieve the list of channels in this team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsIdChannels(id, body, callback)</td>
    <td style="padding:15px">Create a new channel in a Microsoft Team, as specified in the request body.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsIdChannelsId(id, channelId, callback)</td>
    <td style="padding:15px">Retrieve the properties and relationships of a channel.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsIdChannelsId(id, channelId, callback)</td>
    <td style="padding:15px">Delete the channel.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTeamsIdChannelsId(id, channelId, body, callback)</td>
    <td style="padding:15px">Update the properties of the specified channel.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsIdChannelsIdTabsId(id, channelId, tabId, callback)</td>
    <td style="padding:15px">Retrieve the properties and relationships of the specified tab.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/tabs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsIdChannelsIdTabsId(id, channelId, tabId, callback)</td>
    <td style="padding:15px">Removes (unpins) a tab from the specified channel within a team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/tabs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTeamsIdChannelsIdTabsId(id, channelId, tabId, body, callback)</td>
    <td style="padding:15px">Update the properties of the specified tab.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/tabs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsIdInstalledApps(id, appId, callback)</td>
    <td style="padding:15px">Uninstalls an app from the specified team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/installedApps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMeJoinedTeams(callback)</td>
    <td style="padding:15px">Get the teams in Microsoft Teams that the user is a direct member of.</td>
    <td style="padding:15px">{base_path}/{version}/me/joinedTeams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMe(callback)</td>
    <td style="padding:15px">Retrieve the properties and relationships of user object.</td>
    <td style="padding:15px">{base_path}/{version}/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdJoinedTeams(id, callback)</td>
    <td style="padding:15px">Get the teams in Microsoft Teams that the user is a direct member of.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/joinedTeams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrUserPrincipalNameDrive(idOrUserPrincipalName, select, callback)</td>
    <td style="padding:15px">Get a user's OneDrive.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/drive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrUserPrincipalNameOnenoteNotebooks(idOrUserPrincipalName, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of notebook objects.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/onenote/notebooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersIdOrUserPrincipalNameOnenoteNotebooks(idOrUserPrincipalName, body, callback)</td>
    <td style="padding:15px">Create a new OneNote notebook.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/onenote/notebooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrUserPrincipalNameOnenotePages(idOrUserPrincipalName, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of page objects.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/onenote/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersIdOrUserPrincipalNameOnenotePages(idOrUserPrincipalName, bodyFormData, callback)</td>
    <td style="padding:15px">Create a new OneNote page in the default section of the default notebook.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/onenote/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrPrincipalNameOnenoteSectionGroups(idOrUserPrincipalName, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of sectionGroup objects.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/onenote/sectionGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrPrincipalNameOnenoteSections(idOrUserPrincipalName, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of onenoteSection objects.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/onenote/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrPrincipalNameEventsIdAttachments(idOrUserPrincipalName, eventId, select, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of attachment objects attached to an event.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/events/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersIdOrPrincipalNameEventsIdAttachments(idOrUserPrincipalName, eventId, body, callback)</td>
    <td style="padding:15px">Add an attachment to an event.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/events/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrPrincipalNameDriveItemsId(idOrUserPrincipalName, itemId, expand, select, callback)</td>
    <td style="padding:15px">Retrieve the metadata for a DriveItem in a Drive by file system ID.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/drive/items/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdOrPrincipalNameDriveItemsIdChildren(idOrUserPrincipalName, itemId, expand, orderby, select, skipToken, top, callback)</td>
    <td style="padding:15px">Return a collection of DriveItems in the children relationship of a DriveItem.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/drive/items/{pathv2}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersIdDriveRootDelta(id, expand, select, top, callback)</td>
    <td style="padding:15px">Track changes to a drive and its children over time.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/drive/root/delta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDrivesId(driveId, select, callback)</td>
    <td style="padding:15px">Get a drive by id.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDrivesIdItemsId(driveId, itemId, expand, select, callback)</td>
    <td style="padding:15px">Retrieve the metadata for a DriveItem in a Drive by file system ID.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}/items/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDrives(callback)</td>
    <td style="padding:15px">Get a list of drives.</td>
    <td style="padding:15px">{base_path}/{version}/drives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDrivesIdRootDelta(driveId, callback)</td>
    <td style="padding:15px">Track changes to a drive and its children over time.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}/root/delta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDrivesIdItemsIdChildren(driveId, itemId, callback)</td>
    <td style="padding:15px">Return a collection of DriveItems in the children relationship of a DriveItem.</td>
    <td style="padding:15px">{base_path}/{version}/drives/{pathv1}/items/{pathv2}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesIdOnenoteNotebooks(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of notebook objects.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/onenote/notebooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSitesIdOnenoteNotebooks(id, body, callback)</td>
    <td style="padding:15px">Create a new OneNote notebook.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/onenote/notebooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesIdOnenoteSectionGroups(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of sectionGroup objects.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/onenote/sectionGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesIdOnenoteSections(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of onenoteSection objects.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/onenote/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesIdOnenotePages(id, expand, orderby, callback)</td>
    <td style="padding:15px">Retrieve a list of page objects.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/onenote/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSitesIdOnenotePages(id, bodyFormData, callback)</td>
    <td style="padding:15px">Create a new OneNote page in the default section of the default notebook.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/onenote/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesRoot(callback)</td>
    <td style="padding:15px">Access the root SharePoint site within a tenant.</td>
    <td style="padding:15px">{base_path}/{version}/sites/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectoryDeletedItemsMicrosoftGraphGroup(callback)</td>
    <td style="padding:15px">Retrieve a list of recently deleted groups from deleted items.</td>
    <td style="padding:15px">{base_path}/{version}/directory/deletedItems/microsoft.graph.group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectoryDeletedItemsMicrosoftGraphUser(callback)</td>
    <td style="padding:15px">Retrieve a list of recently deleted users from deleted items.</td>
    <td style="padding:15px">{base_path}/{version}/directory/deletedItems/microsoft.graph.user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectoryDeletedItemsItemId(itemId, callback)</td>
    <td style="padding:15px">Retrieve the properties of a recently deleted item in deleted items.</td>
    <td style="padding:15px">{base_path}/{version}/directory/deletedItems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectoryDeletedItemsItemId(itemId, callback)</td>
    <td style="padding:15px">Permanently deletes an item from deleted items.</td>
    <td style="padding:15px">{base_path}/{version}/directory/deletedItems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDirectoryDeletedItemsItemIdRestore(itemId, callback)</td>
    <td style="padding:15px">Restores a recently deleted item from deleted items.</td>
    <td style="padding:15px">{base_path}/{version}/directory/deletedItems/{pathv1}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsChannelMessages(teamsId, channelId, callback)</td>
    <td style="padding:15px">Get the list of messages in the specified Teams channel</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsChannelMessages(teamsId, channelId, bodyFormData, callback)</td>
    <td style="padding:15px">Create a messages in the specified Teams channel</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/channels/{pathv2}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupTeamsChannelMessages(teamsId, channelId, callback)</td>
    <td style="padding:15px">Get the list of messages in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupTeamsChannelMessages(teamsId, channelId, bodyFormData, callback)</td>
    <td style="padding:15px">Create a messages in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupTeamsChannelMessageById(teamsId, channelId, messageId, callback)</td>
    <td style="padding:15px">Get the specified message in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupTeamsChannelMessageById(teamsId, channelId, messageId, bodyFormData, callback)</td>
    <td style="padding:15px">Update the messages in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupTeamsChannelMessageReplies(teamsId, channelId, messageId, callback)</td>
    <td style="padding:15px">Get replies to the message in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages/{pathv3}/replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupTeamsChannelMessageReplies(teamsId, channelId, messageId, bodyFormData, callback)</td>
    <td style="padding:15px">Create a message replies in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages/{pathv3}/replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupTeamsChannelMessageReplyById(teamsId, channelId, messageId, replyId, callback)</td>
    <td style="padding:15px">Get specific reply to the message in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages/{pathv3}/replies/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGroupTeamsChannelMessageReplyById(teamsId, channelId, messageId, replyId, bodyFormData, callback)</td>
    <td style="padding:15px">Update a message reply in the specified Group Teams channel</td>
    <td style="padding:15px">/beta/teams/{pathv1}/channels/{pathv2}/messages/{pathv3}/replies/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserMessages(mailbox, callback)</td>
    <td style="padding:15px">Get user messages from mailbox</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserMessages(mailbox, bodyFormData, callback)</td>
    <td style="padding:15px">Create a message for a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserMoveMessages(mailbox, messageId, bodyFormData, callback)</td>
    <td style="padding:15px">Move a message</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/messages/{pathv2}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserMessagesFromFolder(mailbox, messageId, callback)</td>
    <td style="padding:15px">Get user messages from mailbox folder</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/mailFolders/{pathv2}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserMessagesFromFolder(mailbox, messageId, bodyFormData, callback)</td>
    <td style="padding:15px">Create user message in mailbox folder</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/mailFolders/{pathv2}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
