
## 0.5.0 [05-26-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!9

---

## 0.4.0 [10-24-2021]

- Add 15 calls - update foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!8

---

## 0.3.5 [06-04-2021]

- Changes to add OAuth properties

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!7

---

## 0.3.4 [03-10-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!6

---

## 0.3.3 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!5

---

## 0.3.2 [02-03-2020]

- Added a call from a pre-existing adapter to create notifications

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!4

---

## 0.3.1 [01-13-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!3

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!2

---

## 0.2.0 [09-17-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!1

---
## 0.1.6 [08-01-2019] & 0.1.5 [08-01-2019] & 0.1.4 [07-31-2019] & 0.1.3 [07-31-2019] & 0.1.2 [07-31-2019] & 0.1.1 [07-25-2019]

- Initial Commit

See commit 95b554d

---
