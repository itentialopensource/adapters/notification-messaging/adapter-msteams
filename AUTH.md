## Authenticating Adapter for Microsoft Teams Adapter 

This document will go through the steps for authenticating the Adapter for Microsoft Teams adapter. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>.

### No Authentication
The Adapter for Microsoft Teams adapter does not require authentication. It uses webhook key that can be passed as a task input. To generate a webhook please follow [Microsoft documentation](https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=dotnet). The first part of the generate webhook will be the host for adapter service config, for example "org.webhook.office.com" and the rest will be the webhook key that need to be passed as the task input.

```json
 "authentication": {
      "auth_method": "no_authentication",
      "username": "user",
      "password": "pass",
      "token": "token",
      "token_timeout": 600000,
      "token_cache": "local",
      "invalid_token_error": 401,
      "auth_field": "header.headers.Authorization",
      "auth_field_format": "Basic {b64}{username}:{password}{/b64}"
  }
```
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify token
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
