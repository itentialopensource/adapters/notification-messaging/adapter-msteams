/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-msteams',
      type: 'Msteams',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Msteams = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Msteams Adapter Test', () => {
  describe('Msteams Class Tests', () => {
    const a = new Msteams(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNotification('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createNotificationWithOptionsBodyParam = {
      text: 'string',
      title: 'string'
    };
    describe('#createNotificationWithOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNotificationWithOptions(createNotificationWithOptionsBodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersId = 'fakedata';
    let usersName = 'fakedata';
    let usersEmail = 'fakedata';

    describe('#getMe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMe((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.businessPhones));
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.givenName);
                assert.equal('string', data.response.jobTitle);
                assert.equal('string', data.response.mail);
                assert.equal('string', data.response.mobilePhone);
                assert.equal('string', data.response.officeLocation);
                assert.equal('string', data.response.preferredLanguage);
                assert.equal('string', data.response.surname);
                assert.equal('string', data.response.userPrincipalName);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              usersId = data.response.id;
              usersName = data.response.displayName;
              usersEmail = data.response.mail;
              saveMockData('Me', 'getMe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let groupCreated = false;
    let groupsId = 'fakedata';
    const groupsName = `testGroup101--${Date.now()}`;
    const groupsPostGroupsBodyParam = {
      displayName: groupsName,
      groupTypes: ['Unified'],
      mailEnabled: true,
      mailNickname: 'testGroup101-nick',
      securityEnabled: false
    };

    describe('#postGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroups(groupsPostGroupsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.deletedDateTime);
                assert.equal('string', data.response.classification);
                assert.equal('string', data.response.createdDateTime);
                assert.equal(true, Array.isArray(data.response.creationOptions));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal(true, Array.isArray(data.response.groupTypes));
                assert.equal('string', data.response.mail);
                assert.equal(false, data.response.mailEnabled);
                assert.equal('string', data.response.mailNickname);
                assert.equal('string', data.response.onPremisesLastSyncDateTime);
                assert.equal('string', data.response.onPremisesSecurityIdentifier);
                assert.equal('string', data.response.onPremisesSyncEnabled);
                assert.equal('string', data.response.preferredDataLocation);
                assert.equal(true, Array.isArray(data.response.proxyAddresses));
                assert.equal('string', data.response.renewedDateTime);
                assert.equal(true, Array.isArray(data.response.resourceBehaviorOptions));
                assert.equal(true, Array.isArray(data.response.resourceProvisioningOptions));
                assert.equal(true, data.response.securityEnabled);
                assert.equal('string', data.response.visibility);
                assert.equal(true, Array.isArray(data.response.onPremisesProvisioningErrors));
                groupsId = data.response.id;
                groupCreated = true;
                done();
              } else {
                runCommonAsserts(data, error);
                groupsId = data.response.id;
                saveMockData('Groups', 'postGroups', 'default', data);
                const createWait = setTimeout(() => {
                  groupCreated = true;
                  clearTimeout(createWait);
                  done();
                }, 10000);
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroups(null, null, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                    assert.equal('object', typeof data.response[1]);
                    assert.equal('object', typeof data.response[2]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroups', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPatchGroupsIdBodyParam = {
      mailNickname: 'Nickname-updated'
    };

    describe('#patchGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.patchGroupsId(groupsId, groupsPatchGroupsIdBodyParam, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'patchGroupsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsId(groupsId, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.deletedDateTime);
                    assert.equal('string', data.response.classification);
                    assert.equal('string', data.response.createdDateTime);
                    assert.equal(true, Array.isArray(data.response.creationOptions));
                    assert.equal('string', data.response.description);
                    assert.equal('string', data.response.displayName);
                    assert.equal(true, Array.isArray(data.response.groupTypes));
                    assert.equal('string', data.response.mail);
                    assert.equal(true, data.response.mailEnabled);
                    assert.equal('string', data.response.mailNickname);
                    assert.equal('string', data.response.onPremisesLastSyncDateTime);
                    assert.equal('string', data.response.onPremisesSecurityIdentifier);
                    assert.equal('string', data.response.onPremisesSyncEnabled);
                    assert.equal('string', data.response.preferredDataLocation);
                    assert.equal(true, Array.isArray(data.response.proxyAddresses));
                    assert.equal('string', data.response.renewedDateTime);
                    assert.equal(true, Array.isArray(data.response.resourceBehaviorOptions));
                    assert.equal(true, Array.isArray(data.response.resourceProvisioningOptions));
                    assert.equal(false, data.response.securityEnabled);
                    assert.equal('string', data.response.visibility);
                    assert.equal(true, Array.isArray(data.response.onPremisesProvisioningErrors));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsIdCheckMemberGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              const groupsGetGroupsIdCheckMemberGroupsBodyParam = {
                groupIds: [groupsId]
              };
              a.postGroupsIdCheckMemberGroups(groupsId, groupsGetGroupsIdCheckMemberGroupsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.groupIds));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'postGroupsIdCheckMemberGroups', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // const groupsPostGroupsIdConversationsBodyParam = {
    //   topic: 'testTopic101',
    //   threads: [
    //     {
    //       posts: [
    //         {
    //           body: {
    //             contentType: 'html',
    //             content: 'What do we know so far?'
    //           },
    //           newParticipants: [
    //             {
    //               emailAddress: {
    //                 name: usersName,
    //                 address: usersEmail
    //               }
    //             }
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    // };
    // let groupsConversationId = 'fakedata';
    // describe('#postGroupsIdConversations - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.postGroupsIdConversations(groupsId, groupsPostGroupsIdConversationsBodyParam, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal(false, data.response.hasAttachments);
    //               assert.equal('string', data.response.id);
    //               assert.equal('string', data.response.lastDeliveredDateTime);
    //               assert.equal('string', data.response.preview);
    //               assert.equal('string', data.response.topic);
    //               assert.equal(true, Array.isArray(data.response.uniqueSenders));
    //               assert.equal(true, Array.isArray(data.response.threads));
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             groupsConversationId = data.response.id;
    //             saveMockData('Groups', 'postGroupsIdConversations', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);

    //   }).timeout(attemptTimeout);
    // });

    describe('#getGroupsIdConversations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              const conversationGroupId = groupsId;
              a.getGroupsIdConversations(conversationGroupId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdConversations', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // describe('#getGroupsIdConversationsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdConversationsId(groupsId, groupsConversationId, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal(false, data.response.hasAttachments);
    //               assert.equal('string', data.response.id);
    //               assert.equal('string', data.response.lastDeliveredDateTime);
    //               assert.equal('string', data.response.preview);
    //               assert.equal('string', data.response.topic);
    //               assert.equal(true, Array.isArray(data.response.uniqueSenders));
    //               assert.equal(true, Array.isArray(data.response.threads));
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdConversationsId', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);

    //   }).timeout(attemptTimeout);
    // });

    let groupsCalendarEventId = 'fakedata';
    const groupsCalendarStartDateTime = Date.parse('2019-07-31T16:55:35.575Z') + 3600 * 1000;
    const groupsCalendarEndDateTime = Date.parse('2019-07-31T16:55:35.575Z') + 3600 * 1000 * 2;
    const groupsPostGroupsIdCalendarEventsBodyParam = {
      subject: 'Go for lunch',
      body: {
        contentType: 'HTML',
        content: 'Does late morning work for you?'
      },
      start: {
        dateTime: new Date(groupsCalendarStartDateTime).toISOString(),
        timeZone: 'Eastern Standard Time'
      },
      end: {
        dateTime: new Date(groupsCalendarEndDateTime).toISOString(),
        timeZone: 'Eastern Standard Time'
      },
      location: {
        displayName: 'Harrys Bar'
      },
      attendees: [
        {
          emailAddress: {
            address: usersEmail,
            name: usersName
          },
          type: 'required'
        }
      ]
    };

    describe('#postGroupsIdCalendarEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdCalendarEvents(groupsId, groupsPostGroupsIdCalendarEventsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.attendees));
                    assert.equal('object', typeof data.response.bodyFormData);
                    assert.equal('string', data.response.bodyPreview);
                    assert.equal(true, Array.isArray(data.response.categories));
                    assert.equal('string', data.response.changeKey);
                    assert.equal('string', data.response.createdDateTime);
                    assert.equal('object', typeof data.response.end);
                    assert.equal(true, data.response.hasAttachments);
                    assert.equal('string', data.response.iCalUId);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.importance);
                    assert.equal(false, data.response.isAllDay);
                    assert.equal(true, data.response.isCancelled);
                    assert.equal(true, data.response.isOrganizer);
                    assert.equal(true, data.response.isReminderOn);
                    assert.equal('string', data.response.lastModifiedDateTime);
                    assert.equal('object', typeof data.response.location);
                    assert.equal(true, Array.isArray(data.response.locations));
                    assert.equal('string', data.response.onlineMeetingUrl);
                    assert.equal('object', typeof data.response.organizer);
                    assert.equal('string', data.response.originalEndTimeZone);
                    assert.equal('string', data.response.originalStart);
                    assert.equal('string', data.response.originalStartTimeZone);
                    assert.equal('object', typeof data.response.recurrence);
                    assert.equal(7, data.response.reminderMinutesBeforeStart);
                    assert.equal(true, data.response.responseRequested);
                    assert.equal('object', typeof data.response.responseStatus);
                    assert.equal('string', data.response.sensitivity);
                    assert.equal('string', data.response.seriesMasterId);
                    assert.equal('string', data.response.showAs);
                    assert.equal('object', typeof data.response.start);
                    assert.equal('string', data.response.subject);
                    assert.equal('string', data.response.type);
                    assert.equal('string', data.response.webLink);
                    assert.equal(true, Array.isArray(data.response.attachments));
                    assert.equal('object', typeof data.response.calendar);
                    assert.equal(true, Array.isArray(data.response.extensions));
                    assert.equal(true, Array.isArray(data.response.instances));
                    assert.equal(true, Array.isArray(data.response.multiValueExtendedProperties));
                    assert.equal(true, Array.isArray(data.response.singleValueExtendedProperties));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupsCalendarEventId = data.response.id;
                  saveMockData('Groups', 'postGroupsIdCalendarEvents', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdCalendarEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdCalendarEvents(groupsId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdCalendarEvents', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPatchGroupsIdCalendarEventsIdBodyParam = {
      location: {
        displayName: 'Conf Room 2'
      }
    };

    describe('#patchGroupsIdCalendarEventsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.patchGroupsIdCalendarEventsId(groupsId, groupsCalendarEventId, groupsPatchGroupsIdCalendarEventsIdBodyParam, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'patchGroupsIdCalendarEventsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdCalendarEventsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdCalendarEventsId(groupsId, groupsCalendarEventId, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdCalendarEventsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsCalendarViewStartDateTime = new Date(groupsCalendarStartDateTime - 3600 * 1000).toISOString();
    const groupsCalendarViewSEndDateTime = new Date(groupsCalendarEndDateTime + 3600 * 1000 * 3).toISOString();

    describe('#getGroupsIdCalendarView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdCalendarView(groupsId, groupsCalendarViewStartDateTime, groupsCalendarViewSEndDateTime, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.attendees));
                    assert.equal('object', typeof data.response.bodyFormData);
                    assert.equal('string', data.response.bodyPreview);
                    assert.equal(true, Array.isArray(data.response.categories));
                    assert.equal('string', data.response.changeKey);
                    assert.equal('string', data.response.createdDateTime);
                    assert.equal('object', typeof data.response.end);
                    assert.equal(true, data.response.hasAttachments);
                    assert.equal('string', data.response.iCalUId);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.importance);
                    assert.equal(true, data.response.isAllDay);
                    assert.equal(true, data.response.isCancelled);
                    assert.equal(false, data.response.isOrganizer);
                    assert.equal(false, data.response.isReminderOn);
                    assert.equal('string', data.response.lastModifiedDateTime);
                    assert.equal('object', typeof data.response.location);
                    assert.equal(true, Array.isArray(data.response.locations));
                    assert.equal('string', data.response.onlineMeetingUrl);
                    assert.equal('object', typeof data.response.organizer);
                    assert.equal('string', data.response.originalEndTimeZone);
                    assert.equal('string', data.response.originalStart);
                    assert.equal('string', data.response.originalStartTimeZone);
                    assert.equal('object', typeof data.response.recurrence);
                    assert.equal(8, data.response.reminderMinutesBeforeStart);
                    assert.equal(true, data.response.responseRequested);
                    assert.equal('object', typeof data.response.responseStatus);
                    assert.equal('string', data.response.sensitivity);
                    assert.equal('string', data.response.seriesMasterId);
                    assert.equal('string', data.response.showAs);
                    assert.equal('object', typeof data.response.start);
                    assert.equal('string', data.response.subject);
                    assert.equal('string', data.response.type);
                    assert.equal('string', data.response.webLink);
                    assert.equal(true, Array.isArray(data.response.attachments));
                    assert.equal('object', typeof data.response.calendar);
                    assert.equal(true, Array.isArray(data.response.extensions));
                    assert.equal(true, Array.isArray(data.response.instances));
                    assert.equal(true, Array.isArray(data.response.multiValueExtendedProperties));
                    assert.equal(true, Array.isArray(data.response.singleValueExtendedProperties));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdCalendarView', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // describe('#getGroupsIdDrive - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdDrive(groupsId, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('string', data.response.id);
    //               assert.equal('string', data.response.driveType);
    //               assert.equal('object', typeof data.response.owner);
    //               assert.equal('object', typeof data.response.quota);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdDrive', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // let groupsItemId = 'fakedata';
    // describe('#getGroupsIdDriveRootDelta - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdDriveRootDelta(groupsId, null, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response[0]);
    //               assert.equal('object', typeof data.response[1]);
    //               groupsItemId = data.response[0].id;
    //             } else {
    //               groupsItemId = data.response.value[0].id;
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdDriveRootDelta', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdDriveItemsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdDriveItemsId(groupsId, groupsItemId, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response.createdBy);
    //               assert.equal('string', data.response.createdDateTime);
    //               assert.equal('string', data.response.cTag);
    //               assert.equal('string', data.response.eTag);
    //               assert.equal('object', typeof data.response.folder);
    //               assert.equal('string', data.response.id);
    //               assert.equal('object', typeof data.response.lastModifiedBy);
    //               assert.equal('string', data.response.lastModifiedDateTime);
    //               assert.equal('string', data.response.name);
    //               assert.equal('object', typeof data.response.root);
    //               assert.equal(7, data.response.size);
    //               assert.equal('string', data.response.webUrl);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdDriveItemsId', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdDriveItemsIdChildren - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdDriveItemsIdChildren(groupsId, groupsItemId, null, null, null, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response[0]);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdDriveItemsIdChildren', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    const groupsStartDateTime = Date.parse('2019-07-31T17:55:35.575Z') + 3600 * 1000;
    const groupsEndDateTime = Date.parse('2019-07-31T17:55:35.575Z') + 3600 * 1000 * 2;
    const groupsPostGroupsIdEventsBodyParam = {
      subject: 'Go for lunch2',
      body: {
        contentType: 'HTML',
        content: 'Does late morning work for you?'
      },
      start: {
        dateTime: new Date(groupsStartDateTime).toISOString(),
        timeZone: 'Eastern Standard Time'
      },
      end: {
        dateTime: new Date(groupsEndDateTime).toISOString(),
        timeZone: 'Eastern Standard Time'
      },
      location: {
        displayName: 'Harrys Bar'
      },
      attendees: [
        {
          emailAddress: {
            address: usersEmail,
            name: usersName
          },
          type: 'required'
        }
      ]
    };
    let groupsEventId = 'fakedata';

    describe('#postGroupsIdEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdEvents(groupsId, groupsPostGroupsIdEventsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.attendees));
                    assert.equal('object', typeof data.response.bodyFormData);
                    assert.equal('string', data.response.bodyPreview);
                    assert.equal(true, Array.isArray(data.response.categories));
                    assert.equal('string', data.response.changeKey);
                    assert.equal('string', data.response.createdDateTime);
                    assert.equal('object', typeof data.response.end);
                    assert.equal(true, data.response.hasAttachments);
                    assert.equal('string', data.response.iCalUId);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.importance);
                    assert.equal(true, data.response.isAllDay);
                    assert.equal(false, data.response.isCancelled);
                    assert.equal(true, data.response.isOrganizer);
                    assert.equal(true, data.response.isReminderOn);
                    assert.equal('string', data.response.lastModifiedDateTime);
                    assert.equal('object', typeof data.response.location);
                    assert.equal(true, Array.isArray(data.response.locations));
                    assert.equal('string', data.response.onlineMeetingUrl);
                    assert.equal('object', typeof data.response.organizer);
                    assert.equal('string', data.response.originalEndTimeZone);
                    assert.equal('string', data.response.originalStart);
                    assert.equal('string', data.response.originalStartTimeZone);
                    assert.equal('object', typeof data.response.recurrence);
                    assert.equal(1, data.response.reminderMinutesBeforeStart);
                    assert.equal(true, data.response.responseRequested);
                    assert.equal('object', typeof data.response.responseStatus);
                    assert.equal('string', data.response.sensitivity);
                    assert.equal('string', data.response.seriesMasterId);
                    assert.equal('string', data.response.showAs);
                    assert.equal('object', typeof data.response.start);
                    assert.equal('string', data.response.subject);
                    assert.equal('string', data.response.type);
                    assert.equal('string', data.response.webLink);
                    assert.equal(true, Array.isArray(data.response.attachments));
                    assert.equal('object', typeof data.response.calendar);
                    assert.equal(true, Array.isArray(data.response.extensions));
                    assert.equal(true, Array.isArray(data.response.instances));
                    assert.equal(true, Array.isArray(data.response.multiValueExtendedProperties));
                    assert.equal(true, Array.isArray(data.response.singleValueExtendedProperties));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupsEventId = data.response.id;
                  saveMockData('Groups', 'postGroupsIdEvents', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdEvents(groupsId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                    assert.equal('object', typeof data.response[1]);
                    assert.equal('object', typeof data.response[2]);
                    assert.equal('object', typeof data.response[3]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdEvents', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPatchGroupsIdEventsIdBodyParam = {
      location: {
        displayName: 'Conf Room 2'
      }
    };

    describe('#patchGroupsIdEventsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.patchGroupsIdEventsId(groupsId, groupsEventId, groupsPatchGroupsIdEventsIdBodyParam, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'patchGroupsIdEventsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdEventsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdEventsId(groupsId, groupsEventId, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdEventsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPostGroupsIdEventsIdExtensionsBodyParam = {
      '@odata.type': 'microsoft.graph.openTypeExtension',
      extensionName: 'Com.Contoso.Referral'
    };
    let groupsEventsExtensionId = 'fakedata';

    describe('#postGroupsIdEventsIdExtensions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdEventsIdExtensions(groupsId, groupsEventId, groupsPostGroupsIdEventsIdExtensionsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response['@odata.context']);
                    assert.equal('string', data.response['@odata.type']);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.extensionName);
                    assert.equal('string', data.response.companyName);
                    assert.equal(5, data.response.dealValue);
                    assert.equal('string', data.response.expirationDate);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupsEventsExtensionId = data.response.id;
                  saveMockData('Groups', 'postGroupsIdEventsIdExtensions', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdEventsIdExtensionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdEventsIdExtensionsId(groupsId, groupsEventId, groupsEventsExtensionId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response['@odata.context']);
                    assert.equal('string', data.response['@odata.type']);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.extensionName);
                    assert.equal('string', data.response.companyName);
                    assert.equal(4, data.response.dealValue);
                    assert.equal('string', data.response.expirationDate);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdEventsIdExtensionsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPostGroupsIdExtensionsBodyParam = {
      '@odata.type': 'microsoft.graph.openTypeExtension',
      extensionName: 'Com.Contoso.Deal',
      companyName: 'Alpine Skis',
      dealValue: 1010100,
      expirationDate: '2020-07-03T13:04:00.000Z'
    };
    let groupsExtensionId = 'fakedata';

    describe('#postGroupsIdExtensions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdExtensions(groupsId, groupsPostGroupsIdExtensionsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response['@odata.context']);
                    assert.equal('string', data.response['@odata.type']);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.extensionName);
                    assert.equal('string', data.response.companyName);
                    assert.equal(3, data.response.dealValue);
                    assert.equal('string', data.response.expirationDate);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupsExtensionId = data.response.id;
                  saveMockData('Groups', 'postGroupsIdExtensions', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdExtensionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdExtensionsId(groupsId, groupsExtensionId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response['@odata.context']);
                    assert.equal('string', data.response['@odata.type']);
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.extensionName);
                    assert.equal('string', data.response.companyName);
                    assert.equal(1, data.response.dealValue);
                    assert.equal('string', data.response.expirationDate);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdExtensionsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPostGroupsIdGetMemeberGroupsBodyParam = {
      securityEnabledOnly: false
    };

    describe('#postGroupsIdGetMemeberGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdGetMemeberGroups(groupsId, groupsPostGroupsIdGetMemeberGroupsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.value));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'postGroupsIdGetMemeberGroups', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    const groupsPostGroupsIdGetMemberObjectsBodyParam = {
      securityEnabledOnly: false
    };

    describe('#postGroupsIdGetMemberObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdGetMemberObjects(groupsId, groupsPostGroupsIdGetMemberObjectsBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.value));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'postGroupsIdGetMemberObjects', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsIdMemberRef - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              const groupsMemberOdataId = 'https://graph.microsoft.com/v1.0/directoryObjects/'.concat(usersId);
              const groupsPostGroupsIdMemberRefBodyParam = {
                '@odata.id': groupsMemberOdataId
              };
              a.postGroupsIdMemberRef(groupsId, groupsPostGroupsIdMemberRefBodyParam, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'postGroupsIdMemberRef', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdMemberof - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdMemberof(null, null, null, groupsId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.value));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdMemberof', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdMembers(groupsId, null, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.value));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdMembers', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // const groupsPostGroupsIdOnenoteNotebooksBodyParam = {
    //   displayName: `testNotebook101-${Date.now()}`
    // };
    // describe('#postGroupsIdOnenoteNotebooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.postGroupsIdOnenoteNotebooks(groupsId, groupsPostGroupsIdOnenoteNotebooksBodyParam, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response.createdBy);
    //               assert.equal('string', data.response.createdDateTime);
    //               assert.equal('string', data.response.id);
    //               assert.equal(false, data.response.isDefault);
    //               assert.equal(false, data.response.isShared);
    //               assert.equal('object', typeof data.response.lastModifiedBy);
    //               assert.equal('string', data.response.lastModifiedDateTime);
    //               assert.equal('object', typeof data.response.links);
    //               assert.equal('string', data.response.displayName);
    //               assert.equal('string', data.response.sectionGroupsUrl);
    //               assert.equal('string', data.response.sectionsUrl);
    //               assert.equal('string', data.response.self);
    //               assert.equal('string', data.response.userRole);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'postGroupsIdOnenoteNotebooks', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdOnenoteNotebooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdOnenoteNotebooks(groupsId, null, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response[0]);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdOnenoteNotebooks', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // const groupsBodyFormData = `<!DOCTYPE html>
    // <html>
    //   <head>
    //     <title>A page with <i>rendered</i> images and an <b>attached</b> file</title>
    //     <meta name="created" content="2015-07-22T09:00:00-08:00" />
    //   </head>
    //   <body>
    //     <p>Here's an image from an online source:</p>
    //     <img src="https://..." alt="an image on the page" width="500" />
    //     <p>Here's an image uploaded as binary data:</p>
    //     <img src="name:imageBlock1" alt="an image on the page" width="300" />
    //     <p>Here's a file attachment:</p>
    //     <object data-attachment="FileName.pdf" data="name:fileBlock1" type="application/pdf" />
    //   </body>
    // </html>`;
    // describe('#postGroupsIdOnenotePages - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.postGroupsIdOnenotePages(groupsId, groupsBodyFormData, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('string', data.response.title);
    //               assert.equal('string', data.response.createdByAppId);
    //               assert.equal('object', typeof data.response.links);
    //               assert.equal('string', data.response.contentUrl);
    //               assert.equal('string', data.response.content);
    //               assert.equal('string', data.response.lastModifiedDateTime);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'postGroupsIdOnenotePages', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdOnenotePages - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdOnenotePages(groupsId, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response[0]);
    //               assert.equal('object', typeof data.response[1]);
    //               assert.equal('object', typeof data.response[2]);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdOnenotePages', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdOnenoteSectionGroups - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdOnenoteSectionGroups(groupsId, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response[0]);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdOnenoteSectionGroups', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdOnenoteSections - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdOnenoteSections(groupsId, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response[0]);
    //               assert.equal('object', typeof data.response[1]);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdOnenoteSections', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    describe('#postGroupIdOwnersRef - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              const groupsOwnerOdataId = 'https://graph.microsoft.com/v1.0/users/'.concat(usersId);
              const groupsPostGroupIdOwnersRefBodyParam = {
                '@odata.id': groupsOwnerOdataId
              };
              a.postGroupIdOwnersRef(groupsId, groupsPostGroupIdOwnersRefBodyParam, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'postGroupIdOwnersRef', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsIdOwners - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdOwners(groupsId, null, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.value));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdOwners', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // describe('#getGroupsIdPlannerPlans - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdPlannerPlans(groupsId, null, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('object', typeof data.response.createdBy);
    //               assert.equal('string', data.response.createdDateTime);
    //               assert.equal('string', data.response.id);
    //               assert.equal('string', data.response.owner);
    //               assert.equal('string', data.response.title);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdPlannerPlans', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    describe('#postGroupsIdRenew - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.postGroupsIdRenew(groupsId, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'postGroupsIdRenew', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // const groupsPostGroupsIdSettingsBodyParam = {
    //   displayName: 'testGroupsSettings',
    //   templateId: 'fakedata',
    //   values: [
    //     {
    //       '@odata.type': 'microsoft.graph.settingValue'
    //     }
    //   ]
    // };
    // let groupsSettingId = 'fakedata';
    // describe('#postGroupsIdSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.postGroupsIdSettings(groupsId, groupsPostGroupsIdSettingsBodyParam, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('string', data.response.displayName);
    //               assert.equal('string', data.response.id);
    //               assert.equal('string', data.response.templateId);
    //               assert.equal(true, Array.isArray(data.response.values));
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             groupsSettingId = data.response.id;
    //             saveMockData('Groups', 'postGroupsIdSettings', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getGroupsIdSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdSettings(groupsId, null, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdSettings', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // const groupsPatchGroupsIdSettingsIdBodyParam = {
    //   displayName: 'testGroupsSettings-updated',
    //   templateId: 'fakedata',
    //   values: [
    //     {
    //       '@odata.type': 'microsoft.graph.settingValue'
    //     }
    //   ]
    // };
    // describe('#patchGroupsIdSettingsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.patchGroupsIdSettingsId(groupsId, groupsSettingId, groupsPatchGroupsIdSettingsIdBodyParam, (data, error) => {
    //             if (stub) {
    //               const displayE = 'Error 400 received on request';
    //               runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'patchGroupsIdSettingsId', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getGroupsIdSettingsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.getGroupsIdSettingsId(groupsId, groupsSettingId, null, null, (data, error) => {
    //             runCommonAsserts(data, error);
    //             if (stub) {
    //               assert.equal('string', data.response.displayName);
    //               assert.equal('string', data.response.id);
    //               assert.equal('string', data.response.templateId);
    //               assert.equal(true, Array.isArray(data.response.values));
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'getGroupsIdSettingsId', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    const groupsPutGroupsIdTeamBodyParam = {
      memberSettings: {
        allowCreateUpdateChannels: true
      },
      messagingSettings: {
        allowUserEditMessages: true,
        allowUserDeleteMessages: true
      },
      funSettings: {
        allowGiphy: true,
        giphyContentRating: 'strict'
      }
    };
    let teamsId = 'fakedata';

    describe('#putGroupsIdTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.putGroupsIdTeam(groupsId, groupsPutGroupsIdTeamBodyParam, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.displayName);
                    assert.equal('string', data.response.description);
                    assert.equal('string', data.response.internalId);
                    assert.equal(true, data.response.isArchived);
                    assert.equal('string', data.response.webUrl);
                    assert.equal('object', typeof data.response.guestSettings);
                    assert.equal('object', typeof data.response.memberSettings);
                    assert.equal('object', typeof data.response.messagingSettings);
                    assert.equal('object', typeof data.response.funSettings);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  teamsId = data.response.id;
                  saveMockData('Groups', 'putGroupsIdTeam', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    let groupsThreadId = 'fakedata';
    // const groupsPostGroupsIdThreadsBodyParam = {
    //   topic: 'New Conversation Thread Topic - Test',
    //   posts: [{
    //     body: {
    //       contentType: 'html',
    //       content: 'this is body content'
    //     },
    //     newParticipants: [{
    //       emailAddress: {
    //         name: usersName,
    //         address: usersEmail
    //       }
    //     }]
    //   }]
    // };
    // describe('#postGroupsIdThreads - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postGroupsIdThreads(groupsId, groupsPostGroupsIdThreadsBodyParam, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal(true, Array.isArray(data.response.ccRecipients));
    //           assert.equal(true, data.response.hasAttachments);
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.isLocked);
    //           assert.equal('string', data.response.lastDeliveredDateTime);
    //           assert.equal('string', data.response.preview);
    //           assert.equal(true, Array.isArray(data.response.toRecipients));
    //           assert.equal('string', data.response.topic);
    //           assert.equal(true, Array.isArray(data.response.uniqueSenders));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         groupsThreadId = data.response.id;
    //         saveMockData('Groups', 'postGroupsIdThreads', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getGroupsIdThreads - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdThreads(groupsId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('object', typeof data.response[0]);
                    assert.equal('object', typeof data.response[1]);
                    assert.equal('object', typeof data.response[2]);
                    assert.equal('object', typeof data.response[3]);
                    groupsThreadId = data.response[0].id;
                  } else {
                    runCommonAsserts(data, error);
                    groupsThreadId = data.response.value[0].id;
                  }
                  saveMockData('Groups', 'getGroupsIdThreads', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    // const groupsPatchGroupsIdThreadsIdBodyParam = {
    //   topic: 'Thread test topic - updated'
    // };
    // describe('#patchGroupsIdThreadsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     let running = false;
    //     const interval = setInterval(() => {
    //       if (groupCreated && !running) {
    //         running = true;
    //         clearInterval(interval);
    //         const p = new Promise((resolve) => {
    //           a.patchGroupsIdThreadsId(groupsId, groupsThreadId, groupsPatchGroupsIdThreadsIdBodyParam, (data, error) => {
    //             if (stub) {
    //               const displayE = 'Error 400 received on request';
    //               runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //             } else {
    //               runCommonAsserts(data, error);
    //             }
    //             saveMockData('Groups', 'patchGroupsIdThreadsId', 'default', data);
    //             resolve(data);
    //             done();
    //           });
    //         });
    //         // log just done to get rid of const lint issue!
    //         log.debug(p);
    //       }
    //     }, 1000);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getGroupsIdThreadsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getGroupsIdThreadsId(groupsId, groupsThreadId, null, null, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal(true, Array.isArray(data.response.ccRecipients));
                    assert.equal(true, data.response.hasAttachments);
                    assert.equal('string', data.response.id);
                    assert.equal(false, data.response.isLocked);
                    assert.equal('string', data.response.lastDeliveredDateTime);
                    assert.equal('string', data.response.preview);
                    assert.equal(true, Array.isArray(data.response.toRecipients));
                    assert.equal('string', data.response.topic);
                    assert.equal(true, Array.isArray(data.response.uniqueSenders));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Groups', 'getGroupsIdThreadsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    let teamsAppId = 'fakedata';
    // const teamsPostAppCatalogsTeamAppsBodyParam = {};
    // describe('#postAppCatalogsTeamApps - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postAppCatalogsTeamApps(teamsPostAppCatalogsTeamAppsBodyParam, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.externalId);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.version);
    //           assert.equal('string', data.response.distributionMethod);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         teamsAppId = data.response.id;
    //         saveMockData('Teams', 'postAppCatalogsTeamApps', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getAppCatalogsTeamsApps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppCatalogsTeamsApps(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
                teamsAppId = data.response[0].id;
              } else {
                teamsAppId = data.response.value[0].id;
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getAppCatalogsTeamsApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // const teamsPutAppCatalogsTeamsAppsIdBodyParam = {};
    // describe('#putAppCatalogsTeamsAppsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putAppCatalogsTeamsAppsId(teamsAppId, teamsPutAppCatalogsTeamsAppsIdBodyParam, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Teams', 'putAppCatalogsTeamsAppsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    const teamsPatchTeamsIdBodyParam = {
      memberSettings: {
        allowCreateUpdateChannels: true
      }
    };

    describe('#patchTeamsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTeamsId(teamsId, teamsPatchTeamsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'patchTeamsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsId(teamsId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.internalId);
                assert.equal(true, data.response.isArchived);
                assert.equal('string', data.response.webUrl);
                assert.equal('object', typeof data.response.guestSettings);
                assert.equal('object', typeof data.response.memberSettings);
                assert.equal('object', typeof data.response.messagingSettings);
                assert.equal('object', typeof data.response.funSettings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsPostTeamsIdChannelsBodyParam = {
      displayName: 'testChannel',
      description: 'This is a test channel.'
    };
    let teamsChannelId = 'fakedata';

    describe('#postTeamsIdChannels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTeamsIdChannels(teamsId, teamsPostTeamsIdChannelsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              teamsChannelId = data.response.id;
              saveMockData('Teams', 'postTeamsIdChannels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsIdChannels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsIdChannels(teamsId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsIdChannels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsPatchTeamsIdChannelsIdBodyParam = {
      description: 'This is a test channel - updated.'
    };

    describe('#patchTeamsIdChannelsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTeamsIdChannelsId(teamsId, teamsChannelId, teamsPatchTeamsIdChannelsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'patchTeamsIdChannelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsIdChannelsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsIdChannelsId(teamsId, teamsChannelId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsIdChannelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let teamsTabId = 'fakedata';

    describe('#postTeamsIdChannelsIdTabs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const teamsAppOdataBind = 'https://graph.microsoft.com/v1.0/appCatalogs/teamsApps/'.concat(teamsAppId);
          const teamsPostTeamsIdChannelsIdTabsBodyParam = {
            displayName: 'testChannelTab',
            'teamsApp@odata.bind': teamsAppOdataBind
          };
          a.postTeamsIdChannelsIdTabs(teamsId, teamsChannelId, teamsPostTeamsIdChannelsIdTabsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response['teamsApp@odata.bind']);
                assert.equal('object', typeof data.response.configuration);
                assert.equal('string', data.response.sortOrderIndex);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              teamsTabId = data.response.id;
              saveMockData('Teams', 'postTeamsIdChannelsIdTabs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsIdChannelsIdTabs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsIdChannelsIdTabs(teamsId, teamsChannelId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsIdChannelsIdTabs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsPatchTeamsIdChannelsIdTabsIdBodyParam = {
      displayName: 'testChannelTab-updated'
    };

    describe('#patchTeamsIdChannelsIdTabsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchTeamsIdChannelsIdTabsId(teamsId, teamsChannelId, teamsTabId, teamsPatchTeamsIdChannelsIdTabsIdBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response['teamsApp@odata.bind']);
                assert.equal('object', typeof data.response.configuration);
                assert.equal('string', data.response.sortOrderIndex);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'patchTeamsIdChannelsIdTabsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsIdChannelsIdTabsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsIdChannelsIdTabsId(teamsId, teamsChannelId, teamsTabId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response['teamsApp@odata.bind']);
                assert.equal('object', typeof data.response.configuration);
                assert.equal('string', data.response.sortOrderIndex);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsIdChannelsIdTabsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // const teamsPostTeamsIdCloneBodyParam = {
    //   displayName: 'teamCloned',
    //   description: 'This is a cloned team',
    //   mailNickName: 'clonedTeam',
    //   partsToClone: 'apps,tabs,settings,channels,members',
    //   "visibility": "public"
    // };
    // describe('#postTeamsIdClone - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postTeamsIdClone(teamsId, teamsPostTeamsIdCloneBodyParam, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Teams', 'postTeamsIdClone', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // const teamsPostTeamsIdInstalledAppsBodyParam = {
    //   id: teamsAppId
    // };
    // describe('#postTeamsIdInstalledApps - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postTeamsIdInstalledApps(teamsId, teamsPostTeamsIdInstalledAppsBodyParam, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Teams', 'postTeamsIdInstalledApps', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getTeamsIdInstalledApps - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getTeamsIdInstalledApps(teamsId, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //           assert.equal('object', typeof data.response[1]);
    //           assert.equal('object', typeof data.response[2]);
    //           assert.equal('object', typeof data.response[3]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Teams', 'getTeamsIdInstalledApps', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    const teamsPostTeamsIdArchiveBodyParam = {
      shouldSetSpoSiteReadOnlyForMembers: false
    };

    describe('#postTeamsIdArchive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTeamsIdArchive(teamsId, teamsPostTeamsIdArchiveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postTeamsIdArchive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTeamsIdUnarchive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTeamsIdUnarchive(teamsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postTeamsIdUnarchive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMeJoinedTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMeJoinedTeams((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Me', 'getMeJoinedTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getUsersIdOrUserPrincipalNameDrive - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrUserPrincipalNameDrive(usersIdOrUserPrincipalName, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.driveType);
    //           assert.equal('object', typeof data.response.owner);
    //           assert.equal('object', typeof data.response.quota);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrUserPrincipalNameDrive', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // const usersItemId = 'fakedata';
    // describe('#getUsersIdOrPrincipalNameDriveItemsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrPrincipalNameDriveItemsId(usersIdOrUserPrincipalName, usersItemId, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response.createdBy);
    //           assert.equal('string', data.response.createdDateTime);
    //           assert.equal('string', data.response.cTag);
    //           assert.equal('string', data.response.eTag);
    //           assert.equal('object', typeof data.response.folder);
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.lastModifiedBy);
    //           assert.equal('string', data.response.lastModifiedDateTime);
    //           assert.equal('string', data.response.name);
    //           assert.equal('object', typeof data.response.root);
    //           assert.equal(5, data.response.size);
    //           assert.equal('string', data.response.webUrl);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrPrincipalNameDriveItemsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdOrPrincipalNameDriveItemsIdChildren - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrPrincipalNameDriveItemsIdChildren(usersIdOrUserPrincipalName, usersItemId, null, null, null, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrPrincipalNameDriveItemsIdChildren', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // const usersEventId = 'fakedata';
    // const usersPostUsersIdOrPrincipalNameEventsIdAttachmentsBodyParam = {
    //   name: 'test.txt'
    // };
    // describe('#postUsersIdOrPrincipalNameEventsIdAttachments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postUsersIdOrPrincipalNameEventsIdAttachments(usersIdOrUserPrincipalName, usersEventId, usersPostUsersIdOrPrincipalNameEventsIdAttachmentsBodyParam, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('string', data.response.contentType);
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.isInline);
    //           assert.equal('string', data.response.lastModifiedDateTime);
    //           assert.equal('string', data.response.name);
    //           assert.equal(8, data.response.size);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'postUsersIdOrPrincipalNameEventsIdAttachments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdOrPrincipalNameEventsIdAttachments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrPrincipalNameEventsIdAttachments(usersIdOrUserPrincipalName, usersEventId, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //           assert.equal('object', typeof data.response[1]);
    //           assert.equal('object', typeof data.response[2]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrPrincipalNameEventsIdAttachments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // const usersPostUsersIdOrUserPrincipalNameOnenoteNotebooksBodyParam = {
    //   displayName: `testOnenoteNotebook-${Date.now()}`
    // };
    // describe('#postUsersIdOrUserPrincipalNameOnenoteNotebooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postUsersIdOrUserPrincipalNameOnenoteNotebooks(usersIdOrUserPrincipalName, usersPostUsersIdOrUserPrincipalNameOnenoteNotebooksBodyParam, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response.createdBy);
    //           assert.equal('string', data.response.createdDateTime);
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.isDefault);
    //           assert.equal(false, data.response.isShared);
    //           assert.equal('object', typeof data.response.lastModifiedBy);
    //           assert.equal('string', data.response.lastModifiedDateTime);
    //           assert.equal('object', typeof data.response.links);
    //           assert.equal('string', data.response.displayName);
    //           assert.equal('string', data.response.sectionGroupsUrl);
    //           assert.equal('string', data.response.sectionsUrl);
    //           assert.equal('string', data.response.self);
    //           assert.equal('string', data.response.userRole);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'postUsersIdOrUserPrincipalNameOnenoteNotebooks', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdOrUserPrincipalNameOnenoteNotebooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrUserPrincipalNameOnenoteNotebooks(usersIdOrUserPrincipalName, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrUserPrincipalNameOnenoteNotebooks', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // const usersBodyFormData = `<!DOCTYPE html>
    // <html>
    //   <head>
    //     <title>A page with <i>rendered</i> images and an <b>attached</b> file</title>
    //     <meta name="created" content="2015-07-22T09:00:00-08:00" />
    //   </head>
    //   <body>
    //     <p>Here's an image from an online source:</p>
    //     <img src="https://..." alt="an image on the page" width="500" />
    //     <p>Here's an image uploaded as binary data:</p>
    //     <img src="name:imageBlock1" alt="an image on the page" width="300" />
    //     <p>Here's a file attachment:</p>
    //     <object data-attachment="FileName.pdf" data="name:fileBlock1" type="application/pdf" />
    //   </body>
    // </html>`;
    // describe('#postUsersIdOrUserPrincipalNameOnenotePages - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postUsersIdOrUserPrincipalNameOnenotePages(usersIdOrUserPrincipalName, usersBodyFormData, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.createdByAppId);
    //           assert.equal('object', typeof data.response.links);
    //           assert.equal('string', data.response.contentUrl);
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.lastModifiedDateTime);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'postUsersIdOrUserPrincipalNameOnenotePages', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdOrUserPrincipalNameOnenotePages - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrUserPrincipalNameOnenotePages(usersIdOrUserPrincipalName, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //           assert.equal('object', typeof data.response[1]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrUserPrincipalNameOnenotePages', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdOrPrincipalNameOnenoteSectionGroups - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrPrincipalNameOnenoteSectionGroups(usersIdOrUserPrincipalName, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //           assert.equal('object', typeof data.response[1]);
    //           assert.equal('object', typeof data.response[2]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrPrincipalNameOnenoteSectionGroups', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdOrPrincipalNameOnenoteSections - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdOrPrincipalNameOnenoteSections(usersIdOrUserPrincipalName, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('object', typeof data.response[0]);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdOrPrincipalNameOnenoteSections', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getUsersIdDriveRootDelta - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getUsersIdDriveRootDelta(usersId, null, null, null, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal(true, Array.isArray(data.response.value));
    //           assert.equal('string', data.response['@odata.nextLink']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Users', 'getUsersIdDriveRootDelta', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getUsersIdJoinedTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersIdJoinedTeams(usersId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersIdJoinedTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sitesId = 'fakedata';

    describe('#getSitesRoot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitesRoot((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.createdDateTime);
                assert.equal('string', data.response.lastModifiedDateTime);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              sitesId = data.response.id;
              saveMockData('Sites', 'getSitesRoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitesPostSitesIdOnenoteNotebooksBodyParam = {
      displayName: `testSiteNotebook101-${Date.now()}`
    };

    describe('#postSitesIdOnenoteNotebooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSitesIdOnenoteNotebooks(sitesId, sitesPostSitesIdOnenoteNotebooksBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.createdDateTime);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isDefault);
                assert.equal(true, data.response.isShared);
                assert.equal('object', typeof data.response.lastModifiedBy);
                assert.equal('string', data.response.lastModifiedDateTime);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.sectionGroupsUrl);
                assert.equal('string', data.response.sectionsUrl);
                assert.equal('string', data.response.self);
                assert.equal('string', data.response.userRole);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'postSitesIdOnenoteNotebooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesIdOnenoteNotebooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitesIdOnenoteNotebooks(sitesId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSitesIdOnenoteNotebooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // const sitesBodyFormData = `<!DOCTYPE html>
    // <html>
    //   <head>
    //     <title>A page with <i>rendered</i> images and an <b>attached</b> file</title>
    //     <meta name="created" content="2015-07-22T09:00:00-08:00" />
    //   </head>
    //   <body>
    //     <p>Here's an image from an online source:</p>
    //     <img src="https://..." alt="an image on the page" width="500" />
    //     <p>Here's an image uploaded as binary data:</p>
    //     <img src="name:imageBlock1" alt="an image on the page" width="300" />
    //     <p>Here's a file attachment:</p>
    //     <object data-attachment="FileName.pdf" data="name:fileBlock1" type="application/pdf" />
    //   </body>
    // </html>`;
    // describe('#postSitesIdOnenotePages - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postSitesIdOnenotePages(sitesId, sitesBodyFormData, (data, error) => {
    //         runCommonAsserts(data, error);
    //         if (stub) {
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.createdByAppId);
    //           assert.equal('object', typeof data.response.links);
    //           assert.equal('string', data.response.contentUrl);
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.lastModifiedDateTime);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Sites', 'postSitesIdOnenotePages', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getSitesIdOnenotePages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitesIdOnenotePages(sitesId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSitesIdOnenotePages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesIdOnenoteSectionGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitesIdOnenoteSectionGroups(sitesId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSitesIdOnenoteSectionGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesIdOnenoteSections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitesIdOnenoteSections(sitesId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSitesIdOnenoteSections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let drivesDriveId = 'fakedata';

    describe('#getDrives - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDrives((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
                drivesDriveId = data.response[0].id;
              } else {
                runCommonAsserts(data, error);
                drivesDriveId = data.response.value[0].id;
              }
              saveMockData('Drives', 'getDrives', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDrivesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDrivesId(drivesDriveId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.driveType);
                assert.equal('object', typeof data.response.owner);
                assert.equal('object', typeof data.response.quota);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'getDrivesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let drivesItemId = 'fakedata';

    describe('#getDrivesIdRootDelta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDrivesIdRootDelta(drivesDriveId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                drivesItemId = data.response[0].id;
              } else {
                drivesItemId = data.response.value[0].id;
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'getDrivesIdRootDelta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDrivesIdItemsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDrivesIdItemsId(drivesDriveId, drivesItemId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.createdDateTime);
                assert.equal('string', data.response.cTag);
                assert.equal('string', data.response.eTag);
                assert.equal('object', typeof data.response.folder);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.lastModifiedBy);
                assert.equal('string', data.response.lastModifiedDateTime);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.root);
                assert.equal(2, data.response.size);
                assert.equal('string', data.response.webUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'getDrivesIdItemsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDrivesIdItemsIdChildren - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDrivesIdItemsIdChildren(drivesDriveId, drivesItemId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Drives', 'getDrivesIdItemsIdChildren', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const channelMsg = { key: 'value' };
    describe('#postTeamsChannelMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTeamsChannelMessages(teamsId, teamsChannelId, channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postTeamsChannelMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupTeamsChannelMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupTeamsChannelMessages(teamsId, teamsChannelId, channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postGroupTeamsChannelMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchGroupTeamsChannelMessageById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchGroupTeamsChannelMessageById(teamsId, teamsChannelId, 'fakedata', channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'patchGroupTeamsChannelMessageById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupTeamsChannelMessageReplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupTeamsChannelMessageReplies(teamsId, teamsChannelId, 'fakedata', channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postGroupTeamsChannelMessageReplies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchGroupTeamsChannelMessageReplyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchGroupTeamsChannelMessageReplyById(teamsId, teamsChannelId, 'fakedata', 'fakedata', channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'patchGroupTeamsChannelMessageReplyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsChannelMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsChannelMessages(teamsId, teamsChannelId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsChannelMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupTeamsChannelMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupTeamsChannelMessages(teamsId, teamsChannelId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getGroupTeamsChannelMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupTeamsChannelMessageById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupTeamsChannelMessageById(teamsId, teamsChannelId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getGroupTeamsChannelMessageById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupTeamsChannelMessageReplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupTeamsChannelMessageReplies(teamsId, teamsChannelId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getGroupTeamsChannelMessageReplies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupTeamsChannelMessageReplyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupTeamsChannelMessageReplyById(teamsId, teamsChannelId, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getGroupTeamsChannelMessageReplyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUserMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUserMessages('fakedata', channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postUserMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUserMoveMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUserMoveMessages('fakedata', 'fakedata', channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postUserMoveMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUserMessagesFromFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUserMessagesFromFolder('fakedata', 'fakedata', channelMsg, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postUserMessagesFromFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserMessages('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getUserMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserMessagesFromFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserMessagesFromFolder('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getUserMessagesFromFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupsIdCalendarEventsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroupsIdCalendarEventsId(groupsId, groupsCalendarEventId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deleteGroupsIdCalendarEventsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteGroupsIdConversationsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteGroupsIdConversationsId(groupsId, groupsConversationId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Groups', 'deleteGroupsIdConversationsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteGroupsIdEventsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroupsIdEventsId(groupsId, groupsEventId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deleteGroupsIdEventsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupsIdMembersIdRef - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const groupsMemberId = usersId;
          a.deleteGroupsIdMembersIdRef(groupsId, groupsMemberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deleteGroupsIdMembersIdRef', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupsIdOwnersIdRef - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const groupsOwnerId = usersId;
          a.deleteGroupsIdOwnersIdRef(groupsId, groupsOwnerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deleteGroupsIdOwnersIdRef', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteGroupsIdSettingsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteGroupsIdSettingsId(groupsId, groupsSettingId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Groups', 'deleteGroupsIdSettingsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteGroupsIdThreadsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteGroupsIdThreadsId(groupsId, groupsThreadId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Groups', 'deleteGroupsIdThreadsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteTeamsIdInstalledApps - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteTeamsIdInstalledApps(teamsId, teamsAppId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Teams', 'deleteTeamsIdInstalledApps', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteAppCatalogsTeamAppsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteAppCatalogsTeamAppsId(teamsAppId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }
    //         saveMockData('Teams', 'deleteAppCatalogsTeamAppsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteTeamsIdChannelsIdTabsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsIdChannelsIdTabsId(teamsId, teamsChannelId, teamsTabId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeamsIdChannelsIdTabsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsIdChannelsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsIdChannelsId(teamsId, teamsChannelId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeamsIdChannelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let groupDeleted = false;

    describe('#deleteGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteGroupsId(groupsId, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupDeleted = true;
                  saveMockData('Groups', 'deleteGroupsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryDeletedItemsMicrosoftGraphGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectoryDeletedItemsMicrosoftGraphGroup((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response['@odata.context']);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Directory', 'getDirectoryDeletedItemsMicrosoftGraphGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryDeletedItemsMicrosoftGraphUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectoryDeletedItemsMicrosoftGraphUser((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response['@odata.context']);
                assert.equal(true, Array.isArray(data.response.value));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Directory', 'getDirectoryDeletedItemsMicrosoftGraphUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryDeletedItemsItemId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && groupDeleted && !running) {
            running = true;
            clearInterval(interval);
            try {
              const directoryItemId = groupsId;
              a.getDirectoryDeletedItemsItemId(directoryItemId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.deletedDateTime);
                    assert.equal('string', data.response.classification);
                    assert.equal('string', data.response.createdDateTime);
                    assert.equal(true, Array.isArray(data.response.creationOptions));
                    assert.equal('string', data.response.description);
                    assert.equal('string', data.response.displayName);
                    assert.equal(true, Array.isArray(data.response.groupTypes));
                    assert.equal('string', data.response.mail);
                    assert.equal(true, data.response.mailEnabled);
                    assert.equal('string', data.response.mailNickname);
                    assert.equal('string', data.response.onPremisesLastSyncDateTime);
                    assert.equal('string', data.response.onPremisesSecurityIdentifier);
                    assert.equal('string', data.response.onPremisesSyncEnabled);
                    assert.equal('string', data.response.preferredDataLocation);
                    assert.equal(true, Array.isArray(data.response.proxyAddresses));
                    assert.equal('string', data.response.renewedDateTime);
                    assert.equal(true, Array.isArray(data.response.resourceBehaviorOptions));
                    assert.equal(true, Array.isArray(data.response.resourceProvisioningOptions));
                    assert.equal(false, data.response.securityEnabled);
                    assert.equal('string', data.response.visibility);
                    assert.equal(true, Array.isArray(data.response.onPremisesProvisioningErrors));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Directory', 'getDirectoryDeletedItemsItemId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    let groupRestored = false;

    describe('#postDirectoryDeletedItemsItemIdRestore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && groupDeleted && !running) {
            running = true;
            clearInterval(interval);
            try {
              const directoryItemId = groupsId;
              a.postDirectoryDeletedItemsItemIdRestore(directoryItemId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  if (stub) {
                    assert.equal('string', data.response.id);
                    assert.equal('string', data.response.deletedDateTime);
                    assert.equal('string', data.response.classification);
                    assert.equal('string', data.response.createdDateTime);
                    assert.equal(true, Array.isArray(data.response.creationOptions));
                    assert.equal('string', data.response.description);
                    assert.equal('string', data.response.displayName);
                    assert.equal(true, Array.isArray(data.response.groupTypes));
                    assert.equal('string', data.response.mail);
                    assert.equal(true, data.response.mailEnabled);
                    assert.equal('string', data.response.mailNickname);
                    assert.equal('string', data.response.onPremisesLastSyncDateTime);
                    assert.equal('string', data.response.onPremisesSecurityIdentifier);
                    assert.equal('string', data.response.onPremisesSyncEnabled);
                    assert.equal('string', data.response.preferredDataLocation);
                    assert.equal(true, Array.isArray(data.response.proxyAddresses));
                    assert.equal('string', data.response.renewedDateTime);
                    assert.equal(true, Array.isArray(data.response.resourceBehaviorOptions));
                    assert.equal(true, Array.isArray(data.response.resourceProvisioningOptions));
                    assert.equal(true, data.response.securityEnabled);
                    assert.equal('string', data.response.visibility);
                    assert.equal(true, Array.isArray(data.response.onPremisesProvisioningErrors));
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupRestored = true;
                  saveMockData('Directory', 'postDirectoryDeletedItemsItemIdRestore', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    let groupReDeleted = false;

    describe('#deleteGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && groupRestored && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteGroupsId(groupsId, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  groupReDeleted = true;
                  saveMockData('Groups', 'deleteGroupsId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectoryDeletedItemsItemId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (groupCreated && groupReDeleted && !running) {
            running = true;
            clearInterval(interval);
            try {
              const directoryItemId = groupsId;
              a.deleteDirectoryDeletedItemsItemId(directoryItemId, (data, error) => {
                try {
                  if (stub) {
                    const displayE = 'Error 400 received on request';
                    runErrorAsserts(data, error, 'AD.500', 'Test-msteams-connectorRest-handleEndResponse', displayE);
                  } else {
                    runCommonAsserts(data, error);
                  }
                  saveMockData('Directory', 'deleteDirectoryDeletedItemsItemId', 'default', data);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });
  });
});
