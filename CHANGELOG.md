
## 0.17.4 [10-15-2024]

* Changes made at 2024.10.14_21:25PM

See merge request itentialopensource/adapters/adapter-msteams!27

---

## 0.17.3 [09-05-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-msteams!25

---

## 0.17.2 [08-14-2024]

* Changes made at 2024.08.14_19:43PM

See merge request itentialopensource/adapters/adapter-msteams!24

---

## 0.17.1 [08-07-2024]

* Changes made at 2024.08.06_21:47PM

See merge request itentialopensource/adapters/adapter-msteams!23

---

## 0.17.0 [07-18-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!22

---

## 0.16.7 [03-29-2024]

* Changes made at 2024.03.29_10:21AM

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!21

---

## 0.16.6 [03-21-2024]

* Changes made at 2024.03.21_14:49PM

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!20

---

## 0.16.5 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!19

---

## 0.16.4 [03-13-2024]

* Changes made at 2024.03.13_14:16PM

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!18

---

## 0.16.3 [03-11-2024]

* Changes made at 2024.03.11_11:13AM

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!17

---

## 0.16.2 [02-28-2024]

* Changes made at 2024.02.28_12:33PM

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!16

---

## 0.16.1 [12-23-2023]

* update axious and metadata

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!15

---

## 0.16.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!14

---

## 0.15.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!14

---

## 0.14.0 [10-19-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.13.0 [09-20-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.12.0 [09-19-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.11.0 [09-14-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.10.0 [09-14-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.9.0 [09-14-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.8.0 [09-12-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!13

---

## 0.7.0 [08-16-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!11

---

## 0.6.0 [08-11-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!11

---

## 0.5.1 [08-11-2023]

* Add task with WithOptions

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!12

---

## 0.5.0 [05-26-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!9

---

## 0.4.0 [10-24-2021]

- Add 15 calls - update foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!8

---

## 0.3.5 [06-04-2021]

- Changes to add OAuth properties

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!7

---

## 0.3.4 [03-10-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!6

---

## 0.3.3 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!5

---

## 0.3.2 [02-03-2020]

- Added a call from a pre-existing adapter to create notifications

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!4

---

## 0.3.1 [01-13-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!3

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!2

---

## 0.2.0 [09-17-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-msteams!1

---
## 0.1.6 [08-01-2019] & 0.1.5 [08-01-2019] & 0.1.4 [07-31-2019] & 0.1.3 [07-31-2019] & 0.1.2 [07-31-2019] & 0.1.1 [07-25-2019]

- Initial Commit

See commit 95b554d

---
