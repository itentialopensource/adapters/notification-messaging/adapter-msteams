# Adapter for Microsoft Teams

Vendor: Microsoft
Homepage: https://learn.microsoft.com/

Product: Microsoft Teams
Product Page: https://learn.microsoft.com/en-us/graph/

## Introduction
We classify Microsoft Teams into the Notification domain as it is a business communication platform.

"It is designed to facilitate efficient communication and collaboration within teams and organizations" 
"Teams can organize their conversations into channels" 

## Why Integrate
The MS Teams adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Teams. With this adapter you have the ability to perform operations such as:

- Send Notification 

## Additional Product Documentation
The [API documents for Microsoft Teams](https://learn.microsoft.com/en-us/graph/api/resources/teams-api-overview?view=graph-rest-1.0)